<?php

require 'header.php';
require 'display_action.php';

function display_read($alire) {
	echo $alire->title;
}

?>
<div id="content">

<div id="userpage_avatar">
	<p><img src="<?php echo $user->avatar?>" alt="<?php if(!empty($user->nickname)) echo $user->nickname?>" /></p>
	<p><?php echo $user->nickname ?><?php if($user->admin): ?> - Admin. <?php endif ?></p>
	<?php if($user->nickname === $pseudo): ?>
		<p><a href="<?php echo site_url('user/disconnect') ?>">Se déconnecter</a></p>
	<?php endif ?>
		<?php if ($admin && !$user->admin) {
			echo form_open('user/make_admin');
			echo form_hidden('id', $user->id);
			echo form_hidden('name', $user->nickname);
			echo form_submit('submit', "Rendre {$user->nickname} admin.");
			echo form_close();
		}
		?>
</div>

<div id="lecture">
<?php if (!empty($en_cours)): ?>
	<h2>Papiers à lire :</h2>
	<ul>
		<?php foreach($en_cours as $alire): ?>
		<li><?php display_read($alire); ?></li>
		<?php endforeach ?>
	</ul>
<?php endif ?>

<?php if (!empty($finis)): ?>
	<h2>Papiers lus :</h2>
	<ul>
		<?php foreach($finis as $lu): ?>
		<li><?php display_read($lu); ?></li>
		<?php endforeach ?>
	</ul>
<?php endif ?>
</div>

<?php if($user->id === $id) {
	echo form_open('user/set_avatar');
	echo form_hidden('id', $user->id);
	echo form_label('Avatar') . form_input('avatar', $user->avatar);
	echo form_submit('submit', "Confirmer");
	echo form_close();
}
?>



<?php foreach($actions as $action): ?>
	<p>Il y a <?php echo human_since($action->date) ?> : <?php echo $action->nickname ?> a <?php echo get_action($action->action_type) ?>
    <?php $uri = preg_replace('#\W+#', '_', $action->title); ?>
	« <a href="<?php echo site_url("papers/get/$action->paper_id/$uri") ?>"><?php echo $action->title ?></a> ».</p>
<?php endforeach ?>
</div>
<?php require 'footer.php'; ?>
