<?php

function get_action($action) {
	switch($action) {
		case 'comment': return "commenté"; break;
		case 'submit': return "envoyé"; break;
		case 'edit': return "édité"; break;
		case 'voteup': return "voté pour"; break;
		case 'votedown': return "voté contre"; break;
		case 'readlater': return "décidé de lire"; break;
		case 'readdone': return "fini de lire"; break;
		default: return "FIXME ('$action')"; break;
	}
}

?>
