<?php

require 'header.php';
require 'display_action.php';

?>
<div id="content">

<h2>Activité récente</h2>
<p><strong>Note&nbsp;:</strong> Le site est en lecture seule depuis mai 2017.</p>

<?php foreach($actions as $action): ?>
	<p>Il y a <?php echo human_since($action->date) ?> : <?php echo $action->nickname ?> a <?php echo get_action($action->action_type) ?>
    <?php $uri = preg_replace('#\W+#', '_', $action->title); ?>
	« <a href="<?php echo site_url("papers/get/$action->paper_id/$uri") ?>"><?php echo $action->title ?></a> ».</p>
<?php endforeach ?>

<p>Et bien d'autres... :)</p>
</div>
<?php require 'footer.php'; ?>
