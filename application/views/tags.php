<?php
require 'header.php';

$tag_cloud = array();

foreach($tags as $some_tags) {
	foreach(explode(", ", $some_tags->tags) as $one_tag) {
		if (!isset($tag_cloud[$one_tag]))
			$tag_cloud[$one_tag] = 0;
		$tag_cloud[$one_tag]++;
	}
}
ksort($tag_cloud);

echo "<div id='content'><p>";
foreach ($tag_cloud as $name => $amount) {
	if ($amount > 1) {
		$px = 14 + $amount * 2;
		$url = site_url("papers/tag/$name");
		echo "<a href=\"$url\" style=\"font-size:{$px}px\">$name</a></span> ";
	}
}
echo "</p><p>";

foreach ($tag_cloud as $name => $amount) {
	if ($amount == 1) {
		$px = 14 + $amount * 2;
		$url = site_url("papers/tag/$name");
		echo "<a href=\"$url\" style=\"font-size:{$px}px\">$name</a></span> ";
	}
}
echo "</p></div>";

include 'footer.php';
?>
