<?php require 'header.php'; ?>

<div id="content">
<h2>À propos d'Alexandria</h2>

<p><strong>Note&nbsp;:</strong> Le site est en lecture seule depuis mai 2017. Les informations ci-dessous ne sont plus à jour.</p>

<h3>C'est quoi ?</h3>

<p>Alexandria est un site communautaire d'échange de publications scientifiques (papiers). L'objectif est de favoriser la discussion entre les membres. Il est facile de créer un nouveau papier, et surtout de poser des questions, d'échanger, de <strong>réagir</strong>.</p>

<h3>Quand venir ?</h3>

<p>Si vous avez un papier et que vous souhaitez le faire partager ou le commenter, postez-le, envoyez le lien à d'autres membres, et cela vous permettra d'avoir un support pour la discussion.</p>

<h3>Données</h3>

<p>Les données contribuées sont placées sous <a href="http://creativecommons.org/licenses/by-sa/3.0/">Licence Creative Commons BY-SA</a>. Il est possible d'accéder à un dump de <a href="<?php echo site_url("export/papers/all")?>">tous les papiers</a> et de <a href="<?php echo site_url("export/commentaires/all") ?>">tous les commentaires</a>.</p>

<h3>Fonctionnalités</h3>

<p>Pour l'instant, le nombre de features est limité. J'accepte les contributions et je fonctionne à la motivation extérieure, donc si vous avez besoin d'une feature donnée, contactez moi à quentin point pradet sur gmail. Parmi les possibilités :</p>
<ul>
	<li>intégration avec un système de collection de papiers existant ;</li>
	<li>développement de bookmarklet permettant de faire un import en un clic ;</li>
	<li>fonctionnalités avancées d'édition (citer quelqu'un, aperçu, aperçu temps réel) ;</li>
	<li>séparation des réactions et des commentaires ;</li>
	<li>commentaires hiérarchiques ;</li>
	<li>fonctionnalité de recherche avancée ;</li>
	<li>page présentant l'activité récente sur le site ;</li>
	<li>export autre que rss ;</li>
	<li>recommandation de papiers intéressants ;</li>
	<li>...</li>
</ul>
</div>


<?php include 'footer.php'; ?>
