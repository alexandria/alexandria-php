<?php include 'header.php' ?>
			<div id="papers"> 
				<div class="pagination">
					<p><?php echo $this->pagination->create_links(); ?></p>
				</div>
				<?php
					include 'display_paper.php';
					foreach($papers as $paper) {
						display_paper($paper, $pseudo, $admin, array('num_comments' => TRUE));
					}
				?>
				<div class="pagination">
					<p><?php echo $this->pagination->create_links(); ?></p>
				</div>
			</div>
<?php include 'footer.php' ?>
