<?php
function display_paper($paper, $pseudo = '', $admin = 0, $options = array()) {
	$paper_link = isset($options['link']) && $options['link'] === TRUE;
	$num_comments = isset($options['num_comments']) && $options['num_comments'] === TRUE;
?>
				<!--<div class="navigationarrows">
					<p><a class="haut" href="#"); ?>">&uarr;</a>
					<a class="bas" href="#"); ?>">&darr;</a></p>
				</div>-->
				<div class="paper"> 
					<div class="authorstitle">
						<?php if($paper_link) { $link = $paper->download_url; } else { $link = site_url(array('papers/get', $paper->id, $paper->uri)); } ?>
						<h2><a href="<?php echo $link; ?>">
								<?php echo $paper->title ?>
							</a>
						</h2> 
						<p class="author"> 
						<?php foreach(explode(", ", $paper->authors) as $author): ?>
							<a href="<?php echo site_url(array('papers/author', $author)) ?>"><?php echo $author ?></a> 
						<?php endforeach ?>
						<?php if(!empty($paper->year)) { echo "($paper->year)"; } ?>
						</p> 
					</div>
					<!--<div class="votes"><p>
						<a class="plus" href="<?php echo site_url("papers/upvote/{$paper->id}"); ?>">+</a>
						<span class="vote_value"><?php echo $paper->points ?></span>
						<a class="moins" href="<?php echo site_url("papers/downvote/{$paper->id}"); ?>">-</a>
					</p></div>-->
					<p class="tags"> 
						<?php foreach(explode(", ", $paper->tags) as $tag): ?>
							<a href="<?php echo site_url(array('papers/tag', $tag)) ?>"><?php echo $tag ?></a> 
						<?php endforeach ?>
					</p> 

					<div class="content"> 
						<p><?php echo $paper->description_mdown ?></p> 
						<?php if ($num_comments === TRUE):
							switch($paper->num_comments) {
							case 0: $comment_text = "Pas de commentaire"; break;
							case 1: $comment_text = "Un commentaire"; break;
							default: $comment_text = "{$paper->num_comments} commentaires";break;
							}
?>
							<p class="num_comments"><a href="<?php echo $link ?>"><?php echo $comment_text ?></a></p>
						<?php endif ?>
						<p class="download">
						<?php if (empty($pseudo)) {
								echo "Lire <a href='{$paper->download_url}'>maintenant</a> ?";
						} else {
							$read_done = site_url(array('papers/readdone', $paper->id, $paper->uri));
							$read_later = site_url(array('papers/readlater', $paper->id, $paper->uri));
							if(!empty($paper->download_url)) {
								if(!isset($paper->status) || $paper->status === 'UNKNOWN') {
									echo "Lire <a href='{$paper->download_url}'>maintenant</a> ou
									<a href='$read_later'>plus tard</a> ? <a href='$read_done'>Déjà fait ?</a>";
								} else {
									switch($paper->status) {
									case 'READ':
										$finished = human_since($paper->finished);
										echo "Déjà lu depuis $finished ! <a href='{$paper->download_url}'>Relire ?</a>";
										break;
									case 'TOREAD':
										$since = human_since($paper->since);
										echo "<a href='{$paper->download_url}'>À lire</a> depuis $since.
											<a href='$read_done'>C'est fait ?</a>";
										break;
									}
								}
							}
						}
						?>
						</p>
					</div>
					<div class="clear"></div>
				</div> 
<?php
}
?>
