<?php require 'header.php'; ?>
<?php
	echo form_open('papers/submit_edit');
	echo form_fieldset("Édition d'un papier");
	echo form_hidden('id', $paper->id);
	echo form_label("Titre") . form_input('titre', $paper->title) . '<br />';
	echo form_label("Auteurs") . form_input('auteurs', $paper->authors) . '<br />';
	echo form_label("Année") . form_input('annee', $paper->year) . '<br />';
	echo form_label("Tags") . form_input('tags', $paper->tags) . '<br />';
	echo form_label("URL") . form_input('url', $paper->download_url) . '<br />';
	echo form_label("Description") . form_textarea(array('name' => 'description', 'value' => $paper->description, 'style' => 'width: 100%')) . '<br />';
	echo form_submit('submit', 'Envoyer');
	echo form_fieldset_close();
	echo form_close();
	echo mdown_cheatsheet();
?>


<?php require 'footer.php'; ?>
