<?php include 'header.php' ?>
			<div id="papers"> 
			<?php
				include 'display_paper.php';
				display_paper($paper, $pseudo, $admin, array('link' => TRUE));
			?>
			</div>
			<?php
			$comment_ids = array();
			foreach($comments as $comment):
			$comment_ids[] = intval($comment->id);
			?>
			<div class="comment" id="<?php echo $comment->id; ?>"> 
				<p class="author">
					<a href="<?php echo site_url("user/id/{$comment->author}/{$comment->nickname}"); ?>"><?php echo $comment->nickname ?></a>
					<?php if(!empty($comment->avatar)): ?>
						<img src="<?php echo $comment->avatar ?>" alt="<?php echo $comment->nickname; ?>" />
					<?php endif ?>
					<?php if($comment->author === $id): ?>
						<a href="<?php echo site_url("comment/edit/{$comment->id}"); ?>">Éditer</a>
					<?php endif ?>
				</p> 
				<p><?php echo $comment->content_mdown ?></p>
				<div class="clear"></div> 
			</div> 
			<?php endforeach ?>

			</div> 
<?php include 'footer.php' ?>
