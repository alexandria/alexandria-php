<?php
	$id = $this->session->userdata('id');
	$pseudo = $this->session->userdata('pseudo');
	$admin = $this->session->userdata('admin');
?>
<!DOCTYPE html>
<html>
	<head> 
		<title>Alexandria</title> 
		<link rel="stylesheet" href="<?php echo base_url() ?>public/design.css" type="text/css"> 
		<link href="<?php echo site_url('export/papers/all') ?>" type="application/atom+xml" rel="alternate" title="Flux des papiers" />
		<link href="<?php echo site_url('export/commentaires/all') ?>" type="application/atom+xml" rel="alternate" title="Flux des commentaires" />

		<meta charset="utf-8">
	</head> 
	<body> 
		<div id="page"> 
			<div id="head"> 
				<h1><a href="<?php echo site_url(); ?>">Alexandria</a></h1> 
				<ul> 
					<li><a href="<?php echo site_url('papers'); ?>">Papiers</a></li> 
					<li><a href="<?php echo site_url('tags'); ?>">Tags</a></li> 
					<li><a href="<?php echo site_url('actions'); ?>">Actions</a></li> 
					<li><a href="<?php echo site_url("about"); ?>">À propos</a></li>
				</ul> 
				<div class="clear"></div> 
			</div> 

