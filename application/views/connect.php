<?php
require 'header.php';

echo form_open('user/login');
echo form_fieldset("S'identifier");
echo form_label('Pseudo') . form_input('pseudo') . '<br />';
echo form_label('Mot de passe') . form_password('pass') . '<br />';
echo form_submit('submit', 'Go');
echo form_fieldset_close(); 
echo form_close();

echo form_open('user/register');
echo form_fieldset("S'enregistrer");
echo form_label('Pseudo') . form_input('pseudo') . '<br />';
echo form_label('Mot de passe (6 caractères minimum)') . form_password('pass') . '<br />';
echo form_submit('submit', 'Go');
echo form_fieldset_close(); 
echo form_close();

include 'footer.php';
?>
