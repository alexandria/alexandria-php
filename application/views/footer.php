		</div>
		<div id="footer"><p><a href="<?php echo site_url("about");?>">Alexandria</a> est un <a href="https://gitlab.com/alexandria/alexandria-php">logiciel libre</a>.</p></div>
	<script type="text/javascript" src="<?php echo base_url() ?>public/mootools-alexandria.js"></script>
	<?php // ugly hack to say "if we have comments" 
	if(isset($comment_ids)): ?>
	<script type="text/javascript">
		// press 'n' or 'j' to read next comment (N/J for next unread)
		var nextUnreadCodes = [74, 78];
		var nextCodes = [110, 106];
		var prevCodes = [107, 112];
		var paper = <?php echo $paper->id ?>;

		var colors = {'read': '#fff', 'unread': '#dfd', 'toread': '#fdd'};

		/* Circumvent the "values are strings" limitation */
		Storage.prototype.setObject = function(key, value) { this.setItem(key, JSON.stringify(value)); }
		Storage.prototype.getObject = function(key) { return this.getItem(key) && JSON.parse(this.getItem(key));}

		/* Try to circumvent the "only set a whole object" limitation */
		function setC(k, v) {
			comments = localStorage.getObject(paper);
			if(comments == null) comments = {};
			comments[k] = v;
			localStorage.setObject(paper, comments);
		}
		/* get comments with given status, eg. 'unread' */
		function getC(k) { return localStorage.getObject(paper)[k]; }
		function getAllC() {
			var comments = localStorage.getObject(paper);
			var C = []
			for(name in comments) C.append(comments[name]);
			return C.sort();
		}

		function findC(id) {
			var comments = localStorage.getObject(paper);
			for(name in comments) {
				if(comments[name].indexOf(id) != -1)
					return name;
			}
			return null;
		}

		/* Utility function to change the status of a comment */
		function update(comment, before, after) {
			setC(before, (getC(before).erase(comment)));
			setC(after, (getC(after).append([comment])));
		}

		if(localStorage.getItem(paper) === null) {
			// initialize the database
			localStorage.setObject(paper, {'unread': [], 'read': [], 'toread': []});
		} else {
			// no 'unread' comments, switch everything to 'read'
			getC('unread').each(function(c) { update(c, 'unread', 'read'); });
		}

		// give every comment to the client, let him see if there are new ones
		add_comments(<?php echo json_encode($comment_ids); ?>);

		// checks if there are new unread comments
		function add_comments(all_comments) {
			var unread = all_comments.filter(function(id) {
				return findC(id) == null;
			});
			setC('unread', (unread));
		}

		function clickedComment(e) {
			var id = parseInt(this.getAttribute('id'));
			if(getC('toread').indexOf(id) == -1)
				var from = findC(id), to = 'toread';
			else
				var from = 'toread', to = 'read';
			update(id, from, to);
			this.style.backgroundColor = colors[to];
		}

		divs = $$('.comment').each(function(comment) {
			// colors the comment if it's unread
			var id = comment.getAttribute('id');
			document.id(id).style.backgroundColor = colors[findC(parseInt(id))];

			// add a click event
			comment.addEvent('click', clickedComment);

		});

		function scrollToComment(c) {
			var scrollFx = new Fx.Scroll(window);
			if(findC(c) === 'unread') {
				update(c, 'unread', 'read');
				scrollFx.addEvent('complete', function() {
					// once finished, fade the comment to white
					new Fx.Tween(String(c)).start('background-color', colors['read']);
				});
			}
			scrollFx.toElement($(String(c)), 'y');
		}

		// the user wants the next paper
		document.addEventListener('keypress', function(e) {
			// don't do anything if we're editing a comment
			if ($(e.target).get('tag') === 'textarea')
				return;

			if (nextCodes.indexOf(e.which) != -1) {
				var scrollY = window.getScroll().y;
				var allC = getAllC();
				for(var i = 0; i < allC.length; i++) {
					c = allC[i];
					if($(String(c)).getPosition().y > scrollY) {
						scrollToComment(c);
						break;
					}
				}
				e.preventDefault();
			} else if(prevCodes.indexOf(e.which) != -1) {
				var scrollY = window.getScroll().y;
				var allC = getAllC().reverse();
				for(var i = 0; i < allC.length; i++) {
					c = allC[i];
					if($(String(c)).getPosition().y < scrollY) {
						scrollToComment(c);
						break;
					}
				}
				e.preventDefault();
			} else if (nextUnreadCodes.indexOf(e.which)  != -1) {
				/* get candidate */
				var firstUnread = getC('unread').min();
				if(firstUnread === Infinity)
					return;

				/* update db and change the user's view */
				scrollToComment(firstUnread);

				e.preventDefault();
			}
		});
	</script>
	<?php endif ?>
	</body>
</html>
