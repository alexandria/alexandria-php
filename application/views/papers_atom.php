<?php
	header('Content-Type: application/atom+xml; charset=utf-8');
	echo '<?xml version="1.0" encoding="utf-8"?>'
?>

<feed xmlns="http://www.w3.org/2005/Atom">
	<title>Alexandria - Derniers papiers</title>
	<link href="http://alexandria.fr/export/papers/all" rel="self" />
	<link href="http://alexandria.fr/" />
	<updated><?= date(DateTime::ATOM, strtotime($papers[0]->updated)) ?></updated>
	<id>http://alexandria.fr/papers</id>

<?php foreach($papers as $paper): ?>

<entry>
	<title><?php echo $paper->title ?></title>
	<link href="<?php echo site_url(array('papers/get', $paper->id, $paper->uri)) ?>" />
	<updated><?php echo date(DateTime::ATOM, strtotime($paper->updated)); ?></updated>
	<id>tag:alexandria.fr,2010:<?php echo "$data_type/{$paper->id}"; ?></id>
	<author><name><?php echo $paper->authors ?></name></author>
	<content type="html"><?php echo htmlspecialchars(trim($paper->description_mdown)) ?></content>
</entry>

<?php endforeach ?>

</feed>
