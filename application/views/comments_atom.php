<?php
	header('Content-Type: application/atom+xml; charset=utf-8');
	echo '<?xml version="1.0" encoding="utf-8"?>'
?>

<feed xmlns="http://www.w3.org/2005/Atom">
	<title>Alexandria - Derniers commentaires</title>
	<link href="http://alexandria.fr/export/commentaires/all" rel="self" />
	<link href="http://alexandria.fr/" />
	<updated><?= date(DateTime::ATOM, strtotime($comments[0]->updated)) ?></updated>
	<id>http://alexandria.fr/comments</id>

<?php foreach($comments as $comment): ?>
	<entry>
		<id>tag:alexandria.fr,2010:<?php echo "$data_type/{$comment->id}"; ?></id>
		<updated><?php echo date('c', strtotime($comment->updated)); ?></updated>
		<author><name><?php echo $comment->nickname; ?></name></author>
		<title>Re : <?php echo $comment->title ?></title>
		<content type="html"><?php echo htmlspecialchars(trim($comment->content_mdown)) ?></content>
	</entry>
<?php endforeach ?>

</feed>
