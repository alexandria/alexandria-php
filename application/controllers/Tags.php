<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tags extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model('Paper');
	}

	public function index() {
		$data['tags'] = $this->Paper->get_all_tags();
		$this->load->view('tags', $data);
	}

}

/* End of file tags.php */
/* Location: ./application/controllers/tags.php */
