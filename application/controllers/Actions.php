<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Actions extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model('MUser');
	}
		

	public function index() {
		$data['actions'] = $this->MUser->get_actions();
		$this->load->view('actions', $data);
	}

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
