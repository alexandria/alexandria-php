<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model('MUser');
		$this->load->helper(array('form'));
		$this->load->library(array('form_validation'));
	}
		

	public function connected() {
		$pseudo = $this->session->userdata('pseudo');
		return !empty($pseudo);
	}

	public function index() {
		if (!$this->connected()) {
			redirect('user/connect');
		} else {
			$id = $this->session->userdata('id');
			$pseudo = $this->session->userdata('pseudo');
			redirect('user/id/' . $id . '/' . $pseudo);
		}
	}

	public function id($id, $name) {
		$data['user'] = $this->MUser->get_user_by_id($id);
		if($data['user']->nickname !== $name)
			redirect('/');

		$data['en_cours'] = $this->MUser->get_readlist($id, 'TOREAD');
		$data['finis'] = $this->MUser->get_readlist($id, 'READ');
		$data['actions'] = $this->MUser->get_actions($data['user']->id);
		$this->load->view('user', $data);
	}


	function make_admin() {
		$this->load->helper(array('form'));
		$this->load->library(array('form_validation'));
		$this->form_validation->set_rules('id', 'Identifiant', 'required|integer');
		$this->form_validation->set_rules('name', 'Pseudo', 'required');
		if ($this->form_validation->run() == FALSE) {
			redirect('papers');
		} else {
			$id = intval($this->input->post('id'));
			$name = $this->input->post('name');
			$this->MUser->make_admin($id);
			redirect('user/id/' . $id . '/' . $name);
		}
	}

	function set_avatar() {
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', 'Identifiant', 'required|integer');
		$this->form_validation->set_rules('avatar', 'Pseudo', 'required');
		if ($this->form_validation->run() == FALSE) {
			redirect('papers');
		} else {
			$id = intval($this->input->post('id'));
			$avatar = $this->input->post('avatar');
			$this->MUser->set_avatar($id, $avatar);
			$user = $this->MUser->get_user_by_id($id);
			redirect('user/id/' . $id . '/' . $user->nickname);
		}

	}

	private function ensure_connected() {
		$pseudo = $this->session->userdata('pseudo');
		if(empty($pseudo)) redirect('user/connect');
	}
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
