<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(array('Paper', 'MComment'));
	}

	/* "list" functions */
	// first papers
	public function index()
	{
		$this->papers();
	}
	// first papers
	public function papers($asked = 25) {
		$given = $asked === 'all' ? -1 : 25;
		$data['papers'] = $this->Paper->get_papers(0, $given);
		$data['data_type'] = 'paper';
		$this->load->view('papers_atom', $data);
	}
	// all comments
	public function commentaires($asked = 25)
	{
		$given = $asked === 'all' ? -1 : 25;
		$data['comments'] = $this->MComment->get_comments($given);
		$data['data_type'] = 'comz';
		$this->load->view('comments_atom', $data);
	}


}

/* End of file export.php */
/* Location: ./application/controllers/export.php */
