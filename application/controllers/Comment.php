<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model('Paper');
	}

	private function ensure_connected() {
		$pseudo = $this->session->userdata('pseudo');
		if(empty($pseudo))
			redirect('papers');
	}



}

/* End of file comment.php */
/* Location: ./application/controllers/comment.php */
