<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Papers extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Paper');
		$this->load->model('Action');
	}

	/* "list" functions */

	// pagination helper
	private function init_pagination($config) {
		$this->load->library('pagination');

		$config['per_page'] = '5'; 
		$this->pagination->initialize($config); 
	}
	
	// all papers
	public function index($start = 0)
	{
		$config['base_url'] = site_url('papers/index');
		$config['total_rows'] = $this->Paper->num_papers();
		$this->init_pagination($config);

		$data['papers'] = $this->Paper->get_papers($start, 5);
		$this->load->view('papers', $data);
	}

	// papers from $name
	public function author($name, $start = 0)
	{
		$config['base_url'] = site_url("papers/author/$name");
		$config['total_rows'] = $this->Paper->num_papers(array('authors' => $name));
		// paginate on papers/author/name/*number*, ie. fourth segment
		$config['uri_segment'] = 4;
		$this->init_pagination($config);

		$data['papers'] = $this->Paper->get_author($name, $start, 5);
		$this->load->view('papers', $data);
	}

	// papers tagged $name
	public function tag($name, $start = 0)
	{
		$config['base_url'] = site_url("papers/tag/$name");
		$config['total_rows'] = $this->Paper->num_papers(array('tags' => $name));
		$config['uri_segment'] = 4;
		$this->init_pagination($config);

		$data['papers'] = $this->Paper->get_tag($name, $start, 5);
		$this->load->view('papers', $data);
	}

	public function get($id, $uri)
	{
		$this->load->helper(array('form', 'mdown_cheatsheet'));
		$data['paper'] = $this->Paper->get_paper($id);
		// redirect to the correct URL if it is wrong
		if($data['paper']->uri !== $uri) {
			redirect("papers/get/$id/{$data['paper']->uri}");
		}
		$data['comments'] = $this->Paper->get_comments($id);
		$this->load->view('paper', $data);
	}

	private function ensure_connected() {
		$pseudo = $this->session->userdata('pseudo');
		if(empty($pseudo))
			redirect('papers');
	}

	public function delete($id) {
		$pseudo = $this->session->userdata('pseudo');
		$admin = $this->session->userdata('admin');
		if(empty($pseudo) || $admin !== 1)
			redirect('papers');

		$this->Paper->delete($id);
		redirect('papers');
	}

	/* Votes */
	public function upvote($id) {
		$user_id = $this->session->userdata('id');
		if(empty($user_id)) {
			redirect("user");
		}
		$this->Paper->check_vote($id, $user_id, 1);
		$this->Action->action($id, 'vote');
		$paper = $this->Paper->get_paper($id);
		redirect("papers/get/{$paper->id}/{$paper->uri}");
	}

	public function downvote($id) {
		$user_id = $this->session->userdata('id');
		if(empty($user_id)) {
			redirect("user");
		}
		$this->Paper->check_vote(intval($id), $user_id, -1);
		$this->Action->action($id, 'vote');
		$paper = $this->Paper->get_paper($id);
		redirect("papers/get/{$paper->id}/{$paper->uri}");
	}

	/* Reading list */
	public function readlater($id, $uri) {
		$user_id = $this->session->userdata('id');
		$this->Paper->readlater($id, $user_id);
		$this->Action->action($id, 'readlater');
		redirect("papers/get/$id/$uri");
	}

	public function readdone($id, $uri) {
		$user_id = $this->session->userdata('id');
		$this->Paper->readdone($id, $user_id);
		$this->Action->action($id, 'readdone');
		redirect("papers/get/$id/$uri");
	}

}

/* End of file papers.php */
/* Location: ./application/controllers/papers.php */
