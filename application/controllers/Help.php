<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Help extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}
		

	public function mdown() {
		$this->load->helper('mdown_cheatsheet');
		$this->load->view('mdown_help');
	}

}

/* End of file help.php */
/* Location: ./application/controllers/help.php */

