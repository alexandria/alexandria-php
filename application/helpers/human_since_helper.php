<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * human_since
 *
 * Returns the time elapsed in user-friendly format ("one second ago")
 *
 * @access	public
 * @param	when the date of the event
 * @return	string time elapsed between now and the event
 */
if ( ! function_exists('human_since'))
{
	function human_since($mysql_date)
	{
		$when = new DateTime($mysql_date);
		$now = new DateTime();
		$diff = $when->diff($now);
		$d = $diff;

		$intervals = array('an' => $d->y, 'mois' => $d->m, 'jour' => $d->d,
			'heure' => $d->h, 'minute' => $d->i, 'seconde' => $d->s);

		foreach ($intervals as $unit => $value) {
			if ($value !== 0) {
				$display_unit = $unit === 'mois' || $value === 1 ? $unit : "{$unit}s";
				$invert = $d->invert ? '-' : '';
				return "$value $invert$display_unit";
			}
		}
		return "pas longtemps";
	}
}

// ------------------------------------------------------------------------
/* End of file human_since.php */
/* Location: ./application/helpers/human_since.php */
