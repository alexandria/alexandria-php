<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * mdownize
 *
 * Returns mdownized content of a string
 *
 * @access	public
 * @param	string the original text
 * @return	string
 */
if ( ! function_exists('mdownize'))
{
	function mdownize($text)
	{
		$CI =& get_instance();
		$mdown = $CI->config->item('mdown_path');

		$filename = '/tmp/' . md5($text);
		file_put_contents($filename . '.txt', $text);
		$output = shell_exec("$mdown -o {$filename}.mdown -f xhtml {$filename}.txt");
		$mdown_text = file_get_contents($filename . '.mdown');

		unlink("$filename.txt");
		unlink("$filename.mdown");
		return $mdown_text;
	}
}

// ------------------------------------------------------------------------
/* End of file mdown_helper.php */
/* Location: ./application/helpers/mdown_helper.php */
