<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * mdown_cheatsheet
 *
 * Displays a mdown cheatsheet with a pinch of JS to only show it when necessary
 *
 * @access	public
 */
if ( ! function_exists('mdow_cheatsheet'))
{
	function mdown_cheatsheet($hidden = TRUE)
	{
		if($hidden) {
			echo <<<EOT
<script type="text/javascript"> 
function displayhide(id) {
    e = document.getElementById(id);
    if ( e.style.display == 'none' ) {
        e.style.display='block';
    } else {
        e.style.display='none';
    }
}
</script>
<p class="mdown-show"><a href="/help/mdown" target="_blank" onclick="displayhide('mdown-help'); return false;">Aide-mémoire mdown</a></p>
<div id="mdown-help" style="display:none">
EOT;
		} else {
			echo '<div id="mdown-help"><h2>Aide-mémoire mdown</h2>';
		}
		echo <<<EOT
<table>
	<thead><tr><td>Source</td><td>Rendu</td></tr></thead>
	<tbody>
		<tr><td>/emphase/</td><td><em>emphase</em></td></tr>
		<tr><td>*important*</td><td><strong>important</strong></td></tr>
		<tr><td>A\*</td><td>A*</td></tr>
		<tr><td>[Example](http://example.com)</td><td><a href="http://example.com/">Example</a></td></tr>
		<tr><td><pre> - premier
 - second</pre></td><td><ul><li>premier</li><li>second</li></td></tr>
		<tr><td>`code inline`</td><td><code>code inline</code></td></tr>
		<tr><td><pre>=== Sous-section</pre></td><td><h3>Sous-section</h3></td></tr>
		<tr><td>&gt; Citation</td><td><blockquote>Citation</blockquote></td></tr>
	</tbody>
	</table>
</div>
EOT;

	}
}

// ------------------------------------------------------------------------
/* End of file mdown_helper.php */
/* Location: ./application/helpers/mdown_helper.php */
