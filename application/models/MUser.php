<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MUser extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

	function get_user_by_id($id) {
		$query = $this->db->get_where('users', array('id' => $id))->result();
		return $query[0];
	}
		
	function get_actions($id = 'all') {
		$this->db->select(array('users.nickname', 'papers.title', 'actions.date', 'actions.action_type', 'papers.id paper_id', 'actions.date'))->from('actions');

		if($id !== 'all') {
			$this->db->where(array('actions.user' => intval($id)));
		}

		$this->db->limit(30, 0);

		$this->db->join('users', 'actions.user = users.id')->join('papers', 'actions.paper = papers.id')->order_by('actions.date', 'DESC');
		return $this->db->get()->result();
	}

	function get_readlist($user_id, $status) {
		$this->db->select(array('papers.title', 'readlist.since', 'readlist.finished'));
		$this->db->where(array('user' => $user_id, 'status' => $status));
		$this->db->join('papers', 'papers.id = readlist.paper');
		return $this->db->get('readlist')->result();
	}
		

	function get_user($pseudo, $pass) {
		$hash = crypt($pass, '$2a$07$doesthisimprovesecurity$');
		$query = $this->db->get_where('users', array('nickname' => $pseudo, 'password' => $hash))->result();
		return $query[0];
	}

	function make_admin($id) {
		$this->db->where('id', $id)->update('users', array('admin' => 1));
	}

	function set_avatar($id, $avatar) {
		$this->db->where('id', $id)->update('users', array('avatar' => $avatar));
	}
}

/* End of file paper.php */
/* Location: ./application/model/paper.php */

