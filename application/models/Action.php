<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Action extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

	function action($paper_id, $action) {
		$infos['paper'] = $paper_id;
		$infos['action_type'] = $action;
		$infos['date'] = date("Y-m-d H:i:s");
		$infos['user'] = $this->session->userdata('id');
		$this->db->insert('actions', $infos);
	}
}

/* End of file actions.php */
/* Location: ./application/model/actions.php */

