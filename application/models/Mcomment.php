<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MComment extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_comments($limit) {
		$this->db->order_by('comments.id', 'desc');
		if($limit !== -1) {
			$this->db->limit($limit, 0);
		}
		$this->db->from('comments');
		$this->db->join('papers', 'comments.paper = papers.id');
		$this->db->join('users', 'comments.author = users.id');
		$this->db->select(array('comments.id', 'users.nickname', 'papers.title', 'comments.content_mdown', 'comments.updated'));
		$query = $this->db->get();
		return $query->result();
	}

	function get_comment($id) {
		$result = $this->db->get_where('comments', array('comments.id' => $id), 1, 0)->result();
		return $result[0];
	}

}

/* End of file mcomment.php */
/* Location: ./application/model/mcomment.php */

