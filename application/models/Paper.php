<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paper extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

	function add_uri($papers) {
		foreach($papers as $paper) {
			$paper->uri = preg_replace('#\W+#', '_', $paper->title);
		}
		return $papers;
	}

	private function generic_get($skip, $limit) {
		$this->db->order_by('papers.id', 'desc');
		if($limit !== -1) {
			$this->db->limit($limit, $skip);
		}
		$select_plus = "";
		if(!empty($user)) {
			$this->db->join('readlist', 'readlist.paper = papers.id AND readlist.user = ' . $user, 'left');
			$select_plus = "readlist.*, ";
		}
		$this->db->join('comments', 'comments.paper = papers.id', 'left');
		$this->db->group_by('papers.id');
		$this->db->select($select_plus . 'papers.*, COUNT(comments.id) as num_comments', FALSE);
	}
	

	function get_papers($skip = 0, $limit = 5) {
		$this->generic_get($skip, $limit);
		return $this->add_uri($this->db->get('papers')->result());
	}

	function get_author($name, $skip = 0, $limit = 5) {
		$this->generic_get($skip, $limit);
		$this->db->like('authors', urldecode($name));
		return $this->add_uri($this->db->get('papers')->result());
	}
		
	function get_tag($tag, $skip = 0, $limit = 5) {
		$this->generic_get($skip, $limit);
		$this->db->like('tags', urldecode($tag));
		return $this->add_uri($this->db->get('papers')->result());
	}

	function get_all_tags() {
		$this->db->select('tags');
		return $this->db->get('papers')->result();
	}

	function num_papers($constraint = array()) {
		$this->db->from('papers');
		if(!empty($constraint))
			$this->db->like($constraint);

		return $this->db->count_all_results();
	}

	function get_paper($id) {
		$user = $this->session->userdata('id');
		$this->db->from('papers');
		$this->db->where('papers.id', $id)->limit(1, 0);
		if(!empty($user))
			$this->db->join('readlist', 'readlist.paper = papers.id AND readlist.user = ' . $user, 'left');
		$papers_uri = $this->add_uri($this->db->get()->result());
		return $papers_uri[0];
	}

	function get_paper_by_comment($id) {
		$query = $this->db->distinct()->select(array('papers.id', 'papers.title'))->from('comments')->where(array('comments.id' => $id), 1, 0)->join('papers', 'comments.paper = papers.id')->get();	
		$papers_uri = $this->add_uri($query->result());
		return $papers_uri[0];
	}

	function get_comments($id) {
		$query = $this->db->select(array('comments.id', 'users.nickname', 'users.avatar', 'comments.author', 'comments.content_mdown'))->from('comments')->where(array('comments.paper' => $id), 1, 0)->join('users', 'comments.author = users.id', 'left')->get();	
		return $query->result();
	}

	function delete($id) {
		$this->db->where('id', $id)->limit('1')->delete('papers');	
	}

	/* Votes */
	function check_vote($paper, $user, $value) {
		$where = array('user' => $user, 'paper' => $paper);
		$this->db->trans_start();
		// check value of current vote
		$votes = $this->db->get_where('votes', $where)->result();
		$diff = 0;
		if(empty($votes)) {
			$insert = $where;
			$insert['vote'] = $value;
			$this->db->insert('votes', $insert);
			$diff = $value;
		} else {
			$this->db->where($where)->limit(1)->update('votes', array('vote' => $value));
			$diff = $value - $votes[0]->vote;
		}

		$this->db->where(array('id' => $paper))->set('points', "points + $diff", FALSE)->update('papers');

		$this->db->trans_complete();
	}

	/* Reading list */
	public function readlater($paper, $user_id) {
		$where = array('paper' => $paper, 'user' => $user_id);
		$this->db->trans_start();
			$id = $this->db->get_where('readlist', $where)->result();
			if(empty($id)) {
				$where['status'] = 'TOREAD';
				$this->db->set('since', 'NOW()', FALSE);
				$this->db->insert('readlist', $where);
			}
		$this->db->trans_complete();
	}

	public function readdone($paper, $user_id) {
		$where = array('paper' => $paper, 'user' => $user_id);
		$id = $this->db->get_where('readlist', $where)->result();
		if(empty($id)) {
			$where['status'] = 'READ';
			$this->db->set('since','NOW()', FALSE);
			$this->db->set('finished','NOW()', FALSE);
			$this->db->insert('readlist', $where);
		} else {
			$this->db->where($where);
			$this->db->set('finished','NOW()', FALSE);
			$this->db->update('readlist', array('status' => 'READ'));
		}
	}
}

/* End of file paper.php */
/* Location: ./application/model/paper.php */

